#help:	@ List available tasks on this project
help:
	@grep -E '[a-zA-Z\.\-]+:.*?@ .*$$' $(MAKEFILE_LIST) | tr -d '#' | awk 'BEGIN {FS = ":.*?@ "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

#test:	@ Run unit tests
test:
	dotnet test tests/UnitTests --nologo --no-restore

#tdd:	@ Run unit tests with watch
tdd:
	dotnet watch --project Greed.sln -- test tests/UnitTests --nologo --no-restore

#run:	@ Execute the program
run:
	dotnet run --project src/Cli

#bench:	@ Run the benchmarks
bench:
	dotnet run --project src/Bench --configuration Release

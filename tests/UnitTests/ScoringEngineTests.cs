using Xunit;
using Domain;
using System.Linq;
using Domain.ScoringRules;
using System.Collections.Generic;

namespace UnitTests;

public class ScoringEngineTests
{
    readonly ScoringEngine _engine = new(ScoringRuleSetFactory.AssembleScoringRules());

    [Theory]
    [InlineData(new int[0])]
    [InlineData(new int[] { 1, 1, 1, 1, 1, 1, 1 })]
    public void GivenWrongNumberOfDice_ThrowsDiceCountException(int[] diceValues)
    {
        var dice = diceValues.Select(v => new Die(1, v)).ToArray();
        Assert.Throws<DiceCountException>(() => _engine.Score(dice));
    }

    [Theory]
    [InlineData(100, new int[] { 1 })]
    [InlineData(50, new int[] { 5 })]
    [InlineData(100, new int[] { 3, 1 })]
    [InlineData(150, new int[] { 5, 1 })]
    [InlineData(250, new int[] { 1, 5, 1 })]
    public void GivenSingleRuleDiceThrow_ReturnsExpectedScore(int expectedScore, int[] diceValues)
    {
        var dice = diceValues.Select(v => new Die(1, v)).ToArray();
        var result = _engine.Score(dice);

        Assert.Equal(expectedScore, result.Score);
    }

    [Theory]
    [InlineData(1000, 1)]
    [InlineData(200, 2)]
    [InlineData(300, 3)]
    [InlineData(400, 4)]
    [InlineData(500, 5)]
    [InlineData(600, 6)]
    public void GivenThreeOfAKindRuleDiceThrow_ReturnsExpectedScore(int expectedScore, int value)
    {
        var diceValues = Enumerable.Repeat(value, 3).ToArray();
        var dice = diceValues.Select(v => new Die(1, v)).ToArray();

        var result = _engine.Score(dice);

        Assert.Equal(expectedScore, result.Score);
    }

    [Theory]
    [InlineData(2000, 1)]
    [InlineData(400, 2)]
    [InlineData(600, 3)]
    [InlineData(800, 4)]
    [InlineData(1000, 5)]
    [InlineData(1200, 6)]
    public void GivenFourOfAKindRuleDiceThrow_ReturnsExpectedScore(int expectedScore, int value)
    {
        var diceValues = Enumerable.Repeat(value, 4).ToArray();
        var dice = diceValues.Select(v => new Die(1, v)).ToArray();

        var result = _engine.Score(dice);

        Assert.Equal(expectedScore, result.Score);
    }

    [Theory]
    [InlineData(4000, 1)]
    [InlineData(800, 2)]
    [InlineData(1200, 3)]
    [InlineData(1600, 4)]
    [InlineData(2000, 5)]
    [InlineData(2400, 6)]
    public void GivenFiveOfAKindRuleDiceThrow_ReturnsExpectedScore(int expectedScore, int value)
    {
        var diceValues = Enumerable.Repeat(value, 5).ToArray();
        var dice = diceValues.Select(v => new Die(1, v)).ToArray();

        var result = _engine.Score(dice);

        Assert.Equal(expectedScore, result.Score);
    }

    [Theory]
    [InlineData(8000, 1)]
    [InlineData(1600, 2)]
    [InlineData(2400, 3)]
    [InlineData(3200, 4)]
    [InlineData(4000, 5)]
    [InlineData(4800, 6)]
    public void GivenSixOfAKindRuleDiceThrow_ReturnsExpectedScore(int expectedScore, int value)
    {
        var diceValues = Enumerable.Repeat(value, 6).ToArray();
        var dice = diceValues.Select(v => new Die(1, v)).ToArray();

        var result = _engine.Score(dice);

        Assert.Equal(expectedScore, result.Score);
    }

    [Theory]
    [InlineData(450, new int[] { 2, 2, 2, 1, 1, 5 })]
    [InlineData(500, new int[] { 3, 3, 3, 2, 2, 2 })]
    [InlineData(300, new int[] { 5, 5, 1, 1, 2 })]
    [InlineData(550, new int[] { 5, 1, 1, 3, 3, 3 })]
    [InlineData(550, new int[] { 2, 2, 2, 2, 1, 5 })]
    public void GivenMultipleRuleDiceThrow_ReturnsExpectedScore(int expectedScore, int[] diceValues)
    {
        var dice = diceValues.Select(v => new Die(1, v)).ToArray();
        var result = _engine.Score(dice);

        Assert.Equal(expectedScore, result.Score);
    }

    [Theory]
    [InlineData(new int[] { 1, 1, 2, 2, 3, 3 })]
    [InlineData(new int[] { 6, 6, 3, 3, 2, 2 })]
    public void GivenThreePairsRuleDiceThrow_ReturnsExpectedScore(int[] diceValues)
    {
        var dice = diceValues.Select(v => new Die(1, v)).ToArray();
        var result = _engine.Score(dice);

        Assert.Equal(800, result.Score);
        Assert.Equal(6, result.ScoringDice.DieCount());
    }

    [Fact]
    public void GivenHigherScoring3PairsRuleWith4OfAKindDiceThrow_Returns3PairsScore()
    {
        var rules = new List<IScoringRule>()
        {
            new ThreePairsScoringRule(1500),
            new NumOfAKindScoringRule(1, 4, 1000),
        };

        // we want to treat this like 3 pairs: two pairs of 1's and a pair of 4's
        // because it's worth more to score it like that.
        var diceValues = new int[] { 1, 1, 1, 1, 4, 4 };
        var dice = diceValues.Select(v => new Die(1, v)).ToArray();

        var engine = new ScoringEngine(rules);
        var result = engine.Score(dice);

        Assert.Equal(1500, result.Score);
        Assert.Equal(6, result.ScoringDice.DieCount());
    }

    [Theory]
    [InlineData(new int[] { 1, 2, 3, 4, 5, 6 })]
    [InlineData(new int[] { 6, 3, 1, 4, 5, 2 })]
    public void GivenStraightRuleDiceThrow_ReturnsExpectedScore(int[] diceValues)
    {
        var dice = diceValues.Select(v => new Die(1, v)).ToArray();
        var result = _engine.Score(dice);

        Assert.Equal(1200, result.Score);
        Assert.Equal(6, result.ScoringDice.DieCount());
    }

    [Theory]
    [InlineData(new int[] { 4 })]
    [InlineData(new int[] { 2, 3, 6, 2, 4, 4 })]
    public void GivenFarkle_ReturnsZeroScore(int[] diceValues)
    {
        var dice = diceValues.Select(v => new Die(1, v)).ToArray();
        var result = _engine.Score(dice);

        Assert.Equal(0, result.Score);
        Assert.Equal(0, result.ScoringDice.DieCount());
    }

    [Theory]
    [InlineData(1100, new int[] { 1, 2, 5, 5, 5, 5 })]
    public void GivenSouthlakeRules_ReturnsExpectedScore(int expectedScore, int[] diceValues)
    {
        var dice = diceValues.Select(v => new Die(1, v)).ToArray();
        var engine = new ScoringEngine(ScoringRuleSetFactory.AssembleSouthlakeRuleSet());

        var result = engine.Score(dice);

        Assert.Equal(expectedScore, result.Score);
    }
}

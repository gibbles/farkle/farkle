using Xunit;
using Domain;

namespace UnitTests;

public class DieTests
{
    [Theory]
    [InlineData(0)]
    [InlineData(7)]
    public void GivenDiceValueOutOfBounds_ThrowsDieValueException(int value)
    {
        Assert.Throws<DieValueException>(() => new Die(1, value));
    }

    [Theory]
    [InlineData(1, 1)]
    [InlineData(2, 6)]
    public void GivenValidDiceValue_ObjectCreatedCorrectly(int id, int value)
    {
        var die = new Die(id, value);

        Assert.Equal(id, die.Id);
        Assert.Equal(value, die.CurrentValue);
    }
}

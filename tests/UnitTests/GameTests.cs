using Xunit;
using Moq;
using Domain;
using System.Linq;

namespace UnitTests;

public class GameTests
{
    private readonly Game _game;
    private readonly Mock<IDieRoller> _roller;

    public GameTests()
    {
        _roller = new Mock<IDieRoller>();
        var rules = ScoringRuleSetFactory.AssembleScoringRules();
        var scoringEngine = new ScoringEngine(rules);
        var options = new GameOptions
        {
            PlayerNames = new string[] { "Mal", "Jayne" },
            WinScore = 1000
        };

        _game = new Game(_roller.Object, scoringEngine, options);
    }

    private void SetupDiceRolls(params int[] rolls)
    {
        var result = _roller.SetupSequence(r => r.Roll(It.IsAny<int>()));

        foreach (var roll in rolls)
            result = result.Returns(new Die(1, roll));
    }

    [Fact]
    public void GivenTwoPlayerNames_HasTwoPlayers()
    {
        Assert.Equal(2, _game.Players.Count());
    }

    [Fact]
    public void GivenTwoPlayerNames_ScoresAreZero()
    {
        Assert.True(_game.Players.All(p => p.Score == 0));
    }

    [Fact]
    public void GivenTwoPlayerNames_FirstPlayerIsCurrent()
    {
        Assert.Equal("Mal", _game.CurrentPlayer.Name);
    }

    private void SetupInitialFarkleRoll()
    {
        SetupDiceRolls(
            4, 2, 2, 6, 3, 3,
            4, 2, 2, 6, 3, 3
        );
    }

    [Fact]
    public void WhenFarkleRolled_SecondPlayerIsCurrent()
    {
        SetupInitialFarkleRoll();

        _game.Roll();

        Assert.Equal("Jayne", _game.CurrentPlayer.Name);
    }

    [Fact]
    public void WhenFarkleRolledTwice_FirstPlayerIsCurrentAgain()
    {
        SetupInitialFarkleRoll();

        _game.Roll();
        _game.Roll();

        Assert.Equal("Mal", _game.CurrentPlayer.Name);
    }

    [Fact]
    public void WhenFarkleRolled_RollStateIsFarkled()
    {
        SetupInitialFarkleRoll();

        var rollResult = _game.Roll();

        Assert.Equal(RollState.Farkled, rollResult.State);
    }

    [Fact]
    public void AtBeginningOfTurn_SixDiceAvailable()
    {
        Assert.Equal(6, _game.AvailableDice.Length);
    }

    private void SetupInitialThreeScoringDiceRoll()
    {
        SetupDiceRolls(
            1, 1, 5, 6, 3, 3,
            1, 5, 6, 3, 3
        );
    }

    [Fact]
    public void AfterRollWithThreeScoringDice_ThreeDiceAvailable()
    {
        SetupInitialThreeScoringDiceRoll();

        _game.Roll();

        Assert.Equal(3, _game.AvailableDice.Length);
    }

    [Fact]
    public void AfterInitialRollWithThreeScoringDice_ScoreIsUnchanged()
    {
        SetupInitialThreeScoringDiceRoll();

        _game.Roll();

        Assert.Equal(0, _game.CurrentPlayer.Score);
    }

    [Fact]
    public void AfterRollWithThreeScoringDice_RollStateIsScored()
    {
        SetupInitialThreeScoringDiceRoll();

        var result = _game.Roll();

        Assert.Equal(RollState.Scored, result.State);
    }

    [Fact]
    public void AfterRollWithThreeScoringDiceAndAnotherRoll_TurnScoreIsSet()
    {
        SetupInitialThreeScoringDiceRoll();

        _game.Roll();
        _game.Roll();

        Assert.Equal(250, _game.TurnScore);
    }

    [Fact]
    public void AfterBank_NextPlayerIsCurrent()
    {
        SetupInitialThreeScoringDiceRoll();
        _game.Roll();

        _game.Bank();

        Assert.Equal("Jayne", _game.CurrentPlayer.Name);
    }

    [Fact]
    public void AfterBank_TurnScoreIsReset()
    {
        SetupInitialThreeScoringDiceRoll();
        _game.Roll();

        _game.Bank();

        Assert.Equal(0, _game.TurnScore);
    }

    [Fact]
    public void AfterBank_PlayerScoreIsUpdated()
    {
        SetupInitialThreeScoringDiceRoll();
        var player = _game.CurrentPlayer;
        _game.Roll();

        _game.Bank();

        Assert.Equal(250, player.Score);
    }

    [Fact]
    public void BankBeforeRoll_ThrowsBankRuleViolatedException()
    {
        SetupInitialThreeScoringDiceRoll();
        // no _game.Roll() before bank!

        Assert.Throws<BankRuleViolationException>(() => _game.Bank());
    }

    private void SetupInitialHotDiceRoll()
    {
        SetupDiceRolls(1, 1, 2, 2, 3, 3);
    }

    [Fact]
    public void AfterRollWithAllScoringDice_SixDiceAvailable()
    {
        SetupInitialHotDiceRoll();

        _game.Roll();

        Assert.Equal(6, _game.AvailableDice.Length);
    }

    [Fact]
    public void AfterRollWithAllScoringDice_RollStateIsHot()
    {
        SetupInitialHotDiceRoll();

        var result = _game.Roll();

        Assert.Equal(RollState.Hot, result.State);
    }

    [Fact]
    public void GivenNewGame_GameIsNotOver()
    {
        Assert.False(_game.IsOver);
    }

    private void SetupWin()
    {
        SetupDiceRolls(1, 1, 1, 2, 3, 4);

        _game.Roll();
        _game.Bank();
    }

    [Fact]
    public void GivenWinScoreReachedByLastPlayer_GameIsOver()
    {
        SetupDiceRolls(
            1, 1, 1, 2, 3, 4,  // hits win score
            1, 1, 1, 1, 1, 1); // beats player 1 in sudden death

        _game.Roll();
        _game.Bank();
        _game.Roll();
        _game.Bank();

        Assert.True(_game.IsOver);
    }

    [Fact]
    public void GivenGameOver_RollThrowsGameOverException()
    {
        SetupDiceRolls(
            1, 1, 1, 2, 3, 4,  // hits win score
            1, 1, 1, 1, 1, 1); // beats player 1 in sudden death

        _game.Roll();
        _game.Bank(); // enter sudden death
        _game.Roll();
        _game.Bank(); // player 2 wins

        Assert.Throws<GameOverException>(() => _game.Roll());
    }

    [Fact]
    public void GivenSuddenDeathLastPlayerLoses_GameIsOver()
    {
        SetupDiceRolls(
            1, 1, 1, 2, 3, 4,  // hits win score
            1, 2, 3, 2, 4, 4); // doesn't beat player 1

        _game.Roll();
        _game.Bank(); // enter sudden death
        _game.Roll();
        _game.Bank(); // game over

        Assert.True(_game.IsOver);
    }

    [Fact]
    public void GivenGameOverLastPlayerLoses_RollThrowsGameOverException()
    {
        SetupDiceRolls(
            1, 1, 1, 2, 3, 4,  // hits win score
            1, 2, 3, 2, 4, 4); // doesn't beat player 1

        _game.Roll();
        _game.Bank(); // enter sudden death
        _game.Roll();
        _game.Bank(); // game over

        Assert.Throws<GameOverException>(() => _game.Roll());
    }

    [Fact]
    public void GivenGameOverLastPlayerFarkles_GameIsOver()
    {
        SetupDiceRolls(
            1, 1, 1, 2, 3, 4,  // hits win score
            2, 2, 3, 4, 6, 6); // farkle

        _game.Roll();
        _game.Bank(); // enter sudden death
        _game.Roll(); // farkle: game over

        Assert.Throws<GameOverException>(() => _game.Roll());
    }

    [Fact]
    public void GivenNotLastPlayerHitsWinScore_SecondPlayerGetsAShot()
    {
        SetupWin();

        Assert.Equal("Jayne", _game.CurrentPlayer.Name);
    }

    [Fact]
    public void GivenNotLastPlayerHitsWinScore_SuddenDeathIsSet()
    {
        SetupWin();

        Assert.True(_game.SuddenDeath);
    }
}

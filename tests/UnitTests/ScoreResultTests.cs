using System.Collections.Generic;
using Xunit;
using Domain;
using System.Linq;

namespace UnitTests;

public class ScoreResultTests
{
    [Fact]
    public static void GivenResultsWithSameScore_MaxIsOneWithFewestDice()
    {
        var results = new List<ScoreResult>()
        {
            new ScoreResult(100, new DiceTray(new DieValueCount(1, 3))),
            new ScoreResult(100, new DiceTray(new DieValueCount(1, 1))),
            new ScoreResult(100, new DiceTray(new DieValueCount(1, 2))),
        };

        var max = results.Max() ?? ScoreResult.Zero;

        Assert.Equal(1, max.ScoringDice.DieCount());
    }
}

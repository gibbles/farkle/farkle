# Greed

From [Coding Dojo](https://codingdojo.org/kata/Greed/)

[Greed (Dice Game)](http://en.wikipedia.org/wiki/Greed_%28dice_game%29)

Write a class Greed with a score() method that accepts an array of die values (up to 6). Scoring rules are as follows:

* A single one (100)
* A single five (50)
* Triple ones `[1,1,1]` (1000)
* Triple twos `[2,2,2]` (200)
* Triple threes `[3,3,3]` (300)
* Triple fours `[4,4,4]` (400)
* Triple fives `[5,5,5]` (500)
* Triple sixes `[6,6,6]` (600)

Note that the scorer should work for any number of dice up to 6.

* Four-of-a-kind (Multiply Triple Score by 2)
* Five-of-a-kind (Multiply Triple Score by 4)
* Six-of-a-kind (Multiply Triple Score by 8)
* Three Pairs `[2,2,3,3,4,4]` (800)
* Straight `[1,2,3,4,5,6]` (1200)

For the purposes of our exercise, we'll assume that the score method returns the highest score possible for that throw. For example, given the throw

[1,2,2,2]

you should return `300`, since the score for a single `1` is `100` and a triple `2` is `200`.

Each die can only be scored once (so single die scores cannot be combined with triple die scores for the same individual die, but for instance four `5`s could count as 1 Triple (500) and 1 Single (50) for a total of 550)--until you implement the four-of-a-kind rule, that is, when the score should be 1000.

It is possible for the same scoring rule to apply multiple times. For example,

[1, 1, 5, 5]

should return `300` since you get `100` for each of the `1`'s and `50` for each of the `5`'s.

The refactoring steps should be fun.

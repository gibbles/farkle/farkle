using Domain.ScoringRules;

namespace Domain;

public class Game
{
    private const int MAX_DICE = 6;

    private readonly Player[] _players;
    public IEnumerable<Player> Players { get => _players; }

    private int _currentPlayerIndex = 0;
    public Player CurrentPlayer { get => _players[_currentPlayerIndex]; }
    public IEnumerable<IScoringRule> Rules { get; }

    public Die[] AvailableDice { get; private set; } = Array.Empty<Die>();

    public int TurnScore { get; private set; }

    public bool CanBank { get => !_isFirstRollOfTurn; }

    private Die[] _dice = Array.Empty<Die>();
    private readonly IDieRoller _roller;
    private readonly IScoringEngine _engine;
    public readonly int WinScore;
    private bool _isFirstRollOfTurn;
    public bool IsOver { get; private set; }

    public int TopScore => _players.Max(p => p.Score);
    public bool SuddenDeath { get; private set; }

    public Game(IDieRoller roller, IScoringEngine engine, GameOptions options)
    {
        _roller = roller;
        _engine = engine;
        Rules = _engine.Rules;
        WinScore = options.WinScore;

        _players = options.PlayerNames.Select(name => new Player(name)).ToArray();

        ResetTurnValues();
    }

    public RollResult Roll()
    {
        if (IsOver)
            throw new GameOverException();

        var currentRollDice = RollAvailableDice();
        var currentRollScore = ScoreRoll(currentRollDice);

        if (Farkled(currentRollScore))
            return HandleFarkleRoll(currentRollDice, currentRollScore);

        UpdateTurnScore();

        // hurts my feelings that _dice can't be set until after UpdateTurnScore()
        // need to calculate the turn score based on the previous roll
        // tough to reason about--need to put some more attention on this method

        _dice = currentRollDice;

        return RolledHotDice(currentRollScore)
            ? HandleHotDiceRoll(currentRollScore)
            : HandleScoringDiceRoll(currentRollScore);
    }

    public void Bank()
    {
        if (!CanBank)
            throw new BankRuleViolationException();

        BankTurnScore();

        StartNextTurn();
    }

    private RollResult HandleFarkleRoll(Die[] currentRollDice, ScoreResult currentRollScore)
    {
        var result = RollResult.Farkled(currentRollDice, currentRollScore);

        UpdateIsOver();

        StartNextTurn();

        return result;
    }

    private RollResult HandleHotDiceRoll(ScoreResult currentRollScore)
    {
        var result = RollResult.Hot(_dice, currentRollScore);

        ResetAvailableDice();

        return result;
    }

    private RollResult HandleScoringDiceRoll(ScoreResult currentRollScore)
    {
        AvailableDice = currentRollScore.ScoringDice.DetermineUnusedDice(_dice);

        return RollResult.Scored(_dice, currentRollScore);
    }

    private ScoreResult ScoreRoll(Die[] dice)
    {
        return _engine.Score(dice);
    }

    private Die[] RollAvailableDice()
    {
        return AvailableDice
            .Select(d => _roller.Roll(d.Id))
            .ToArray();
    }

    private void UpdateTurnScore()
    {
        if (_isFirstRollOfTurn)
        {
            _isFirstRollOfTurn = false;
            return;
        }

        var lastRollScore = ScoreRoll(_dice);
        TurnScore += lastRollScore.Score;
    }

    private static bool Farkled(ScoreResult result)
    {
        return result.Score == 0;
    }

    private bool RolledHotDice(ScoreResult result)
    {
        return result.ScoringDice.DieCount() == AvailableDice.Length;
    }

    private void BankTurnScore()
    {
        // if we stored this on the Roll, we wouldn't need to recalculate. hmmm...
        var lastRollScore = ScoreRoll(_dice);
        TurnScore += lastRollScore.Score;

        CurrentPlayer.BankScore(TurnScore);

        UpdateSuddenDeath();
        UpdateIsOver();
    }

    private void UpdateIsOver()
    {
        IsOver = IsLastPlayer() && SuddenDeath;
    }

    private void UpdateSuddenDeath()
    {
        SuddenDeath = SuddenDeath || CurrentPlayer.Score >= WinScore;
    }

    private bool IsLastPlayer()
    {
        return _currentPlayerIndex == _players.Length - 1;
    }

    private void StartNextTurn()
    {
        if (IsOver)
            return;

        _currentPlayerIndex = (_currentPlayerIndex + 1) % _players.Length;
        ResetTurnValues();
    }

    private void ResetTurnValues()
    {
        TurnScore = 0;
        _isFirstRollOfTurn = true;
        ResetAvailableDice();
    }

    private void ResetAvailableDice()
    {
        AvailableDice = Enumerable.Range(1, MAX_DICE)
            .Select(i => new Die(i, 1))
            .ToArray();
    }
}

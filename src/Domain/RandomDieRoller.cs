namespace Domain;

public class RandomDieRoller : IDieRoller
{
    private readonly Random _rng = new();
    private readonly object _lockObject = new();

    public Die Roll(int dieId)
    {
        lock(_lockObject)
        {
            return new Die(dieId, _rng.Next(Die.MIN_VALUE, Die.MAX_VALUE));
        }
    }
}

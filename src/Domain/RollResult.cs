namespace Domain;

public class RollResult
{
    public RollState State { get; }
    public IEnumerable<Die> Dice { get; }
    public DiceTray ScoringDice { get; }
    public int MaxScore { get; }

    private RollResult(IEnumerable<Die> dice, RollState state, ScoreResult scoreResult)
    {
        State = state;
        Dice = dice;
        MaxScore = scoreResult.Score;
        ScoringDice = scoreResult.ScoringDice;
    }

    public static RollResult Farkled(IEnumerable<Die> dice, ScoreResult scoreResult)
    {
        return new RollResult(dice, RollState.Farkled, scoreResult);
    }

    public static RollResult Scored(IEnumerable<Die> dice, ScoreResult scoreResult)
    {
        return new RollResult(dice, RollState.Scored, scoreResult);
    }

    public static RollResult Hot(IEnumerable<Die> dice, ScoreResult scoreResult)
    {
        return new RollResult(dice, RollState.Hot, scoreResult);
    }
}

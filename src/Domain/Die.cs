namespace Domain;

public class Die
{
    public const int MIN_VALUE = 1;
    public const int MAX_VALUE = 6;
    public const int FACE_COUNT = MAX_VALUE - MIN_VALUE + 1;
    
    public int Id { get; private init;}
    public int CurrentValue { get; private set; }

    public Die(int id, int currentValue)
    {
        if (currentValue is < MIN_VALUE or > MAX_VALUE)
            throw new DieValueException(currentValue, MIN_VALUE, MAX_VALUE);

        Id = id;
        CurrentValue = currentValue;
    }

    public static IEnumerable<int> GetFaces() =>
        Enumerable.Range(MIN_VALUE, FACE_COUNT);
}

namespace Domain;

[Serializable]
public class BankRuleViolationException : Exception
{
    public BankRuleViolationException()
        : base($"You can't bank if you haven't rolled.") { }

    protected BankRuleViolationException(
        System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
}

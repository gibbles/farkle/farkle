namespace Domain;

public class ScoreResult : IComparable<ScoreResult>
{
    public int Score { get; init; }
    public DiceTray ScoringDice { get; init; }

    public static readonly ScoreResult Zero = new(0, DiceTray.Empty);

    public ScoreResult(int score, DiceTray scoringDice)
    {
        Score = score;
        ScoringDice = scoringDice;
    }

    public int CompareTo(ScoreResult? other)
    {
        if (other is null)
            return 1;

        var val = Score.CompareTo(other.Score);

        // if the scores of the two calculations are the same, the "greater" calculation is the one that
        // uses the smallest number of dice (giving more dice for other rules to use)
        if (val == 0)
            val = CompareDieCountsReversed(other);

        return val;
    }

    private int CompareDieCountsReversed(ScoreResult other)
    {
        return ScoringDice.DieCount().CompareTo(other.ScoringDice.DieCount()) * -1;
    }
}

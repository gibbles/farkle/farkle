namespace Domain;

[Serializable]
public class GameOverException : Exception
{
    public GameOverException()
        : base($"The game is over. You can no longer take any actions.") { }

    protected GameOverException(
        System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
}

namespace Domain.ScoringRules;

public class NumOfAKindScoringRule : INumOfAKindScoringRule
{
    public string Name { get; }
    public int DieValue { get; }
    public int MinCount { get; }
    public int Score { get; }

    public NumOfAKindScoringRule(int dieValue, int minCount, int score)
    {
        DieValue = dieValue;
        MinCount = minCount;
        Score = score;
        Name = BuildName();
    }

    private string BuildName()
    {
        return MinCount switch
        {
            1 => DieValue + "s",
            3 => $"Triple {DieValue}s",
            _ => $"{MinCount} {DieValue}s"
        };
    }

    public ScoreResult Calculate(DiceTray tray)
    {
        if (tray.DieCount(DieValue) < MinCount)
            return ScoreResult.Zero;

        var count = new DieValueCount(DieValue, MinCount);
        var scored = new DiceTray(count);

        return new ScoreResult(Score, scored);
    }
}

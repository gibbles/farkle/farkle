namespace Domain.ScoringRules;

public interface IScoringRule
{
    public string Name { get; }
    public int Score { get; }
    public ScoreResult Calculate(DiceTray tray);
}

namespace Domain.ScoringRules;

public class StraightScoringRule : IScoringRule
{
    public int Score { get; }
    public string Name { get; } = "Straight";

    public StraightScoringRule(int score)
    {
        Score = score;
    }

    public ScoreResult Calculate(DiceTray tray)
    {
        // relying on DiceTray being an IEnumerable<DieValueCount>

        if (!tray.All(dieValueCount => dieValueCount.Count == 1))
            return ScoreResult.Zero;

        return new ScoreResult(Score, tray);
    }
}

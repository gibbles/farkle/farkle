namespace Domain.ScoringRules;

public class NumOfAKindSetScoringRule : INumOfAKindScoringRule
{
    public int MinCount { get; }
    public int Score { get; }
    public string Name { get; }

    public NumOfAKindSetScoringRule(int minCount, int score)
    {
        MinCount = minCount;
        Score = score;
        Name = BuildName();
    }

    public ScoreResult Calculate(DiceTray tray)
    {
        var candidate = tray.FirstOrDefault(dvc => dvc.Count >= MinCount);

        if (candidate.Count == 0)
            return ScoreResult.Zero;

        var count = new DieValueCount(candidate.DieValue, MinCount);
        var scored = new DiceTray(count);

        return new ScoreResult(Score, scored);
    }

    private string BuildName()
    {
        return $"{MinCount} of a kind";
    }
}

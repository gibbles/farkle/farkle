namespace Domain.ScoringRules;

public class ThreePairsScoringRule : IScoringRule
{
    public int Score { get; }
    public string Name { get; } = "Three pairs";

    public ThreePairsScoringRule(int score)
    {
        Score = score;
    }

    public ScoreResult Calculate(DiceTray tray)
    {
        // relying on DiceTray being an IEnumerable<DieValueCount>

        var pairs = tray.Where(dvc => HasEvenCount(dvc));

        if (pairs.Sum(dvc => dvc.Count) != 6)
            return ScoreResult.Zero;

        var scoredTray = new DiceTray(pairs.ToArray());

        return new ScoreResult(Score, scoredTray);
    }

    private static bool HasEvenCount(DieValueCount dvc)
    {
        return dvc.Count > 0 && dvc.Count % 2 == 0;
    }
}

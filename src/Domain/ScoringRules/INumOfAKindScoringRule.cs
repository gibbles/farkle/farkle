namespace Domain.ScoringRules;

public interface INumOfAKindScoringRule : IScoringRule
{
    public int MinCount { get; }
}

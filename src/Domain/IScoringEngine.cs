using Domain.ScoringRules;

namespace Domain;

public interface IScoringEngine
{
    IEnumerable<IScoringRule> Rules { get; }
    ScoreResult Score(Die[] diceThrow);
}

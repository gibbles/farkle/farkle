namespace Domain;

public enum RollState
{
    Farkled,
    Scored,
    Hot
}

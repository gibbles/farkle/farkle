namespace Domain;

public class Player
{
    public string Name { get; }

    public int Score { get; private set; }

    public void BankScore(int score)
    {
        Score += score;
    }

    public Player(string name)
    {
        Name = name;
    }
}

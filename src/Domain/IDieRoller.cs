namespace Domain;

public interface IDieRoller
{
    Die Roll(int dieId);
}

namespace Domain;

[Serializable]
public class DieValueException : Exception
{
    public DieValueException(int faceValue, int minValue, int maxValue) :
        base($"Die value must fall into the range {minValue}-{maxValue}. Received {faceValue}.") { }

    public DieValueException(Die[] dice)
        : base($"All die values must fall into the range {Die.MIN_VALUE}-{Die.MAX_VALUE}. Received [{string.Join(", ", dice.Where(d => d.CurrentValue < Die.MIN_VALUE || d.CurrentValue > Die.MAX_VALUE))}].") { }

    protected DieValueException(
        System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
}

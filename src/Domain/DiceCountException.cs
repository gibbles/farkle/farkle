namespace Domain;

[Serializable]
public class DiceCountException : Exception
{
    public DiceCountException(int numberOfDieValues)
        : base($"Received {numberOfDieValues} die values. The count must be in the range {Die.MIN_VALUE}-{Die.MAX_VALUE}.") { }

    protected DiceCountException(
        System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
}

namespace Domain;

public class GameOptions
{
    public IEnumerable<string> PlayerNames { get; init; } = new string[] { "Player 1" };
    public int WinScore { get; init; } = 10000;
}

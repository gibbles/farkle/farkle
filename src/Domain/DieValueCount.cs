namespace Domain;

public readonly struct DieValueCount
{
    public int DieValue { get; }
    public int Count { get; }

    public DieValueCount(int dieValue, int count)
    {
        DieValue = dieValue;
        Count = count;
    }
}

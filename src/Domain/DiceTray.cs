using System.Collections;

namespace Domain;

/// <summary>Embodies the concept of a collection of 6 sided dice that have been rolled and grouped by face value. Immutable.</summary>
public class DiceTray : IEnumerable<DieValueCount>
{
    private readonly Dictionary<int, int> _counts;

    private DiceTray()
    {
        _counts = InitializeDieCounts();
    }

    private DiceTray(Dictionary<int, int> counts)
    {
        _counts = counts;
    }

    public DiceTray(params DieValueCount[] valueCounts): this()
    {
        foreach (var vc in valueCounts)
            _counts[vc.DieValue] = vc.Count;
    }

    public DiceTray(Die[] diceThrow): this()
    {
        PopulateDiceCounts(diceThrow);
    }

    /// <summary>Returns an empty <c>DiceTray</c></summary>
    public static readonly DiceTray Empty = new();

    /// <summary>Returns the total number of dice in the tray of a given face value</summary>
    public int DieCount(int dieValue)
    {
        _counts.TryGetValue(dieValue, out var count);
        return count;
    }

    /// <summary>Returns the total number of dice in the tray</summary>
    public int DieCount()
    {
        return _counts.Sum(p => p.Value);
    }

    public Die[] DetermineUnusedDice(Die[] rolledDice) {
        // this feels really hacky and the whole concept needs to be revisited.
        // The dice tray was never intended to deal with dice that have id's--only values
        var countsRemaining = new Dictionary<int, int>(_counts);
        var unusedDice = new List<Die>();

        foreach(var die in rolledDice)
        {
            if (countsRemaining[die.CurrentValue] > 0)
            {
                countsRemaining[die.CurrentValue] -= 1;
            }
            else
            {
                unusedDice.Add(die);
            }
        }

        return unusedDice.ToArray();
    }

    public static DiceTray operator -(DiceTray minuend, DiceTray subtrahend)
    {
        var counts = new Dictionary<int, int>(Die.FACE_COUNT);
        foreach (var face in Die.GetFaces())
            counts[face] = minuend.DieCount(face) - subtrahend.DieCount(face);

        return new DiceTray(counts);
    }

    public static DiceTray operator +(DiceTray augend, DiceTray addend)
    {
        var counts = new Dictionary<int, int>(Die.FACE_COUNT);
        foreach (var face in Die.GetFaces())
            counts[face] = augend.DieCount(face) + addend.DieCount(face);

        return new DiceTray(counts);
    }

    private static Dictionary<int, int> InitializeDieCounts()
    {
        var counts = new Dictionary<int, int>(Die.FACE_COUNT);
        foreach (var face in Die.GetFaces())
            counts[face] = 0;
        
        return counts;
    }

    private void PopulateDiceCounts(Die[] diceThrow)
    {
        for (var i = 0; i < diceThrow.Length; i++)
            _counts[diceThrow[i].CurrentValue] += 1;
    }

    public IEnumerator<DieValueCount> GetEnumerator()
    {
        return _counts.Select(kvp => new DieValueCount(kvp.Key, kvp.Value)).GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}

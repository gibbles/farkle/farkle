using Domain.ScoringRules;

namespace Domain;

public static class ScoringRuleSetFactory
{
    public static IEnumerable<IScoringRule> AssembleScoringRules()
    {
        return AssembleMansfieldRuleSet();
    }

    /// <summary>The kata-based ruleset</summary>
    public static IEnumerable<IScoringRule> AssembleMansfieldRuleSet()
    {
        var rules = new List<IScoringRule>();

        rules.AddRange(BuildStandardRuleSet());
        rules.AddRange(BuildDoublingMultiNumOfAKindRuleSet());

        rules.Add(new ThreePairsScoringRule(800));
        rules.Add(new StraightScoringRule(1200));

        return rules;
    }

    /// <summary>A simplified ruleset using set scoring for multiples, but with higher scores for the specials</summary>
    public static IEnumerable<IScoringRule> AssembleSouthlakeRuleSet()
    {
        var rules = new List<IScoringRule>();

        rules.AddRange(BuildStandardRuleSet());
        rules.AddRange(BuildSetMultiNumOfAKindRuleSet());

        rules.Add(new ThreePairsScoringRule(1500));
        rules.Add(new StraightScoringRule(2500));

        return rules;
    }

    /// <summary>A slower paced ruleset, with generally lower scores for melds</summary>
    public static IEnumerable<IScoringRule> AssembleDentonRuleSet()
    {
        var rules = new List<IScoringRule>();

        rules.AddRange(BuildStandardRuleSet());
        rules.AddRange(BuildAddingMultiNumOfAKindRuleSet());

        rules.Add(new ThreePairsScoringRule(800));
        rules.Add(new StraightScoringRule(1200));

        return rules;
    }

    private static IEnumerable<IScoringRule> BuildStandardRuleSet()
    {
        var rules = new List<IScoringRule>();

        rules.AddRange(BuildSinglesRuleSet());
        rules.AddRange(BuildTriplesRuleSet());

        return rules;
    }

    private static IEnumerable<IScoringRule> BuildSinglesRuleSet()
    {
        var rules = new List<IScoringRule>()
        {
            new NumOfAKindScoringRule(dieValue: 1, minCount: 1, score: 100),
            new NumOfAKindScoringRule(dieValue: 5, minCount: 1, score: 50),
        };

        return rules;
    }

    private static IEnumerable<IScoringRule> BuildTriplesRuleSet()
    {
        var rules = new List<IScoringRule>();

        foreach(var dieValue in Die.GetFaces())
        {
            var tripleScore = CalculateTripleScore(dieValue);
            rules.Add(new NumOfAKindScoringRule(dieValue, minCount: 3, score: tripleScore));
        }

        return rules;
    }

    private static int CalculateTripleScore(int dieValue)
    {
        const int tripleOneScore = 1000;

        return dieValue == 1
            ? tripleOneScore
            : dieValue * 100;
    }

    private static IEnumerable<IScoringRule> BuildSetMultiNumOfAKindRuleSet()
    {
        return new List<IScoringRule>()
        {
            new NumOfAKindSetScoringRule(minCount: 4, score: 1000),
            new NumOfAKindSetScoringRule(minCount: 5, score: 2000),
            new NumOfAKindSetScoringRule(minCount: 6, score: 3000),
        };
    }

    private static IEnumerable<IScoringRule> BuildDoublingMultiNumOfAKindRuleSet()
    {
        var rules = new List<IScoringRule>();

        foreach(var dieValue in Die.GetFaces())
        {
            var tripleScore = CalculateTripleScore(dieValue);

            rules.Add(new NumOfAKindScoringRule(dieValue, minCount: 4, score: tripleScore * 2));
            rules.Add(new NumOfAKindScoringRule(dieValue, minCount: 5, score: tripleScore * 4));
            rules.Add(new NumOfAKindScoringRule(dieValue, minCount: 6, score: tripleScore * 8));
        }

        return rules;
    }

    private static IEnumerable<IScoringRule> BuildAddingMultiNumOfAKindRuleSet()
    {
        var rules = new List<IScoringRule>();

        foreach(var dieValue in Die.GetFaces())
        {
            var tripleScore = CalculateTripleScore(dieValue);

            rules.Add(new NumOfAKindScoringRule(dieValue, minCount: 4, score: tripleScore * 2));
            rules.Add(new NumOfAKindScoringRule(dieValue, minCount: 5, score: tripleScore * 3));
            rules.Add(new NumOfAKindScoringRule(dieValue, minCount: 6, score: tripleScore * 4));
        }

        return rules;
    }
}

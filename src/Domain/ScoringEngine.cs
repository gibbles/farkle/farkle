﻿using Domain.ScoringRules;

namespace Domain;

public class ScoringEngine : IScoringEngine
{
    public IEnumerable<IScoringRule> Rules { get; }

    public ScoringEngine(IEnumerable<IScoringRule> scoringRules)
    {
        Rules = scoringRules ?? throw new ArgumentNullException(nameof(scoringRules));
    }

    public ScoreResult Score(Die[] diceThrow)
    {
        ValidateDiceThrow(diceThrow);

        return CalculateHighestPossibleScore(new DiceTray(diceThrow));
    }

    private ScoreResult CalculateHighestPossibleScore(DiceTray tray)
    {
        var currentTray = tray;
        var scoringTray = DiceTray.Empty;
        var score = 0;

        // So that we don't have to worry about the order of the rules in the list, we iterate,
        // searching for the highest scoring rule every iteration and removing the scored dice
        // from the passed tray until no more rules will score.
        ScoreResult highest;
        do
        {
            highest = FindHighestRuleScore(currentTray);
            score += highest.Score;
            currentTray -= highest.ScoringDice;
            scoringTray += highest.ScoringDice;
        } while (highest.Score > 0);

        return new ScoreResult(score, scoringTray);
    }

    private ScoreResult FindHighestRuleScore(DiceTray tray)
    {
        return Rules
            .Select(r => r.Calculate(tray))
            .Max() // uses ScoringRuleCalculation as IComparable
            ?? ScoreResult.Zero;
    }

    private static void ValidateDiceThrow(Die[] diceThrow)
    {
        if (diceThrow is null)
            throw new ArgumentNullException(nameof(diceThrow));

        // probably not this class's job to check for greater than 6 dice.
        // the scoring engine is agnostic to to the game engine.
        if (diceThrow.Length is < 1 or > 6)
            throw new DiceCountException(diceThrow.Length);
    }
}

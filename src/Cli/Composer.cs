using Domain;

using Spectre.Console;

namespace Cli;

public class Composer
{
    public readonly Theme Theme;

    private readonly IDieRoller _roller;
    private readonly IScoringEngine _engine;
    private readonly GameOptions _options;

    public Composer()
    {
        _roller = new RandomDieRoller();
        _engine = new ScoringEngine(ScoringRuleSetFactory.AssembleMansfieldRuleSet());
        Theme = new Theme();

        var playerNames = GetPlayers(Theme);

        _options = new GameOptions
        {
            PlayerNames = playerNames,
            WinScore = 10000
        };
    }

    public GameRunner BuildRunner()
    {
        var game = new Game(_roller, _engine, _options);
        return new GameRunner(game, Theme);
    }

    private static IEnumerable<string> GetPlayers(Theme theme)
    {
        string name;
        var names = new List<string>();
        int i = 1;

        AnsiConsole.MarkupLine($"Who's playing? [{theme.GameMasterVoiceColor}](Enter a blank name when you're done.)[/]");

        do
        {
            var prompt = new TextPrompt<string>($"[{theme.SeparatorValueColor}]Player {i}[/] Name: ")
                .AllowEmpty();
            name = AnsiConsole.Prompt(prompt);

            if (!string.IsNullOrWhiteSpace(name))
                names.Add(name);
            i++;
        } while (!string.IsNullOrWhiteSpace(name));

        if (!names.Any())
            names.Add("Anonymous");

        return names;
    }
}

﻿using Spectre.Console;

namespace Cli;

public static class Program
{
    public static void Main()
    {
        var composer = new Composer();

        char playAgain;
        do
        {
            WriteTitle(composer.Theme);

            var disposition = composer.BuildRunner().Run();

            if (disposition == GameDisposition.Quit)
                return;

            playAgain = AskPlayAgain();
        } while (playAgain == 'y');
    }

    private static void WriteTitle(Theme theme)
    {
        AnsiConsole.Write(
            new FigletText("Farkle")
                .Centered()
                .Color(theme.TitleColor)
            );
    }

    private static char AskPlayAgain()
    {
        return AnsiConsole.Ask<string>("Play again?")
            .ToLower()
            .FirstOrDefault();
    }
}

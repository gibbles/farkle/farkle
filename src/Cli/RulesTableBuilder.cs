using Domain.ScoringRules;

using Spectre.Console;
using Spectre.Console.Rendering;

namespace Cli;

public class RulesTableBuilder
{
    private readonly IEnumerable<IScoringRule> _rules;
    private readonly string _color;

    public RulesTableBuilder(IEnumerable<IScoringRule> rules, string color)
    {
        _rules = rules;
        _color = color;
    }

    public Table Build()
    {
        Table table = CreateTable();

        foreach (var rule in _rules
            .Where(r => r is NumOfAKindScoringRule)
            .OrderBy(r => ((NumOfAKindScoringRule)r).MinCount)
            .ThenBy(r => ((NumOfAKindScoringRule)r).DieValue))
        {
            table.AddRow(BuildNumOfAKindRow(rule));
        }

        foreach (var rule in _rules
            .Where(r => r is NumOfAKindSetScoringRule)
            .OrderBy(r => ((NumOfAKindSetScoringRule)r).MinCount)
            .ThenBy(r => r.Score))
        {
            table.AddRow(BuildNumOfAKindRow(rule));
        }

        foreach (var rule in _rules
            .Where(r => r is not INumOfAKindScoringRule)
            .OrderBy(r => r.Score))
        {
            table.AddRow(BuildRow(rule));
        }

        return table;
    }

    private TableRow BuildNumOfAKindRow(IScoringRule rule)
    {
        return new TableRow(new IRenderable[]
        {
            new Markup($"[{_color}]{rule.Name}[/]"),
            new Markup($"[{_color}]{rule.Score:N0}[/]")
        });
    }

    private TableRow BuildRow(IScoringRule rule)
    {
        return new TableRow(new IRenderable[]
        {
            new Markup($"[{_color}]{rule.Name}[/]"),
            new Markup($"[{_color}]{rule.Score:N0}[/]")
        });
    }

    private static Table CreateTable()
    {
        var table = new Table();
        table.Border(TableBorder.Rounded);

        table.AddColumn("Meld");
        var scoreColumn = new TableColumn("Score");
        scoreColumn.RightAligned();
        table.AddColumn(scoreColumn);
        return table;
    }
}

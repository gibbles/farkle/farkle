using Domain;

using Spectre.Console;
using Spectre.Console.Rendering;

namespace Cli;

public class ScoreboardBuilder
{
    private readonly IEnumerable<Player> _players;
    private readonly Dictionary<int, string> _podiumScoreIconLookup;
    private readonly string _color;
    private const string LEADER_ICON = "👑";
    private const string FIRST_PLACE_ICON = "🏆";
    private const string SECOND_PLACE_ICON = "🥈";
    private const string THIRD_PLACE_ICON = "🥉";
    private const string ZERO_ICON = "🍩";
    private const string NO_ICON = " ";

    public ScoreboardBuilder(IEnumerable<Player> players, bool gameOver, string color)
    {
        _color = color;

        _players = gameOver
            ? players.OrderByDescending(p => p.Score)
            : players;

        _podiumScoreIconLookup = gameOver
            ? BuildGameOverIconLookup()
            : BuildActiveGameIconLookup();
    }

    private Dictionary<int, string> BuildActiveGameIconLookup()
    {
        var lookup = new Dictionary<int, string>();

        var firstPlaceScore = _players.Max(p => p.Score);

        lookup[firstPlaceScore] = LEADER_ICON;
        lookup[0] = ZERO_ICON;

        return lookup;
    }

    private Dictionary<int, string> BuildGameOverIconLookup()
    {
        var lookup = new Dictionary<int, string>();

        var scores = _players
            .OrderByDescending(p => p.Score)
            .Select(p => p.Score)
            .Distinct()
            .ToArray();

        if (scores.Length > 0)
            lookup[scores[0]] = FIRST_PLACE_ICON;

        if (scores.Length > 1)
            lookup[scores[1]] = SECOND_PLACE_ICON;

        if (scores.Length > 2)
            lookup[scores[2]] = THIRD_PLACE_ICON;

        lookup[0] = ZERO_ICON;

        return lookup;
    }

    public Table Build()
    {
        Table table = CreateTable();

        foreach (var player in _players)
            table.AddRow(BuildPlayerRow(player));

        return table;
    }

    private TableRow BuildPlayerRow(Player player)
    {
        return new TableRow(new IRenderable[]
        {
            new Markup($"[{_color}]{player.Name}[/]"),
            new Markup(GetHighlightIcon(player.Score)),
            new Markup($"[{_color}]{player.Score:N0}[/]")
        });
    }

    private string GetHighlightIcon(int score)
    {
        _podiumScoreIconLookup.TryGetValue(score, out var icon);
        
        return icon ?? NO_ICON;
    }

    private static Table CreateTable()
    {
        var table = new Table();
        table.Border(TableBorder.Rounded);
        table.HideHeaders();

        table.AddColumn("Player");
        table.AddColumn("");
        var scoreColumn = new TableColumn("Score");
        scoreColumn.RightAligned();
        table.AddColumn(scoreColumn);
        return table;
    }
}

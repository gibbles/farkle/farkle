using Spectre.Console;
namespace Cli;

public class Theme
{
    public Color TitleColor { get; init; } = Color.Red;
    public string SeparatorColor { get; init; } = "grey";
    public string SeparatorLabelColor { get; init; } = "blue";
    public string SeparatorValueColor { get; init; } = "teal";
    public string CommandColor { get; init; } = "red";
    public string TurnScoreColor { get; init; } = "red";
    public string ScoringDiceColor { get; init; } = "teal";
    public string BoringDiceColor { get; init; } = "grey";
    public string FarkleColor { get; init; } = "yellow";
    public string HotDiceColor { get; init; } = "orange1";
    public string GameMasterVoiceColor { get; init; } = "blue";
    public string SuddenDeathColor { get; init; } = "orange1";
    public string SuddenDeathScoreColor { get; init; } = "yellow";
}

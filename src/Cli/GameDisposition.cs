namespace Cli;

public enum GameDisposition
{
    Finished,
    Quit
}

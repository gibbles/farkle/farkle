using System.Text;

using Domain;

namespace Cli;

public class DiceArtStringBuilder
{
    private const string TOP_LINE = "╭─────╮";
    private const string NO_PIP = "│     │";
    private const string LEFT_PIP = "│ ●   │";
    private const string CENTER_PIP = "│  ●  │";
    private const string RIGHT_PIP = "│   ● │";
    private const string DOUBLE_PIP = "│ ● ● │";
    private const string BOTTOM_LINE = "╰─────╯";
    private const string SPACER = "  ";
    private const string CLOSE_TAG = "[/]";

    private string _scoringColor = "";
    private string _boringColor = "";
    private string _textColor = "";
    private string _text = "";
    private readonly Die[] _dice;
    private readonly bool[] _scoringFlags;

    public DiceArtStringBuilder(Theme theme, RollResult result)
    {
        _dice = result.Dice
            .OrderBy(d => d.CurrentValue)
            .ToArray();
        _scoringFlags = new bool[_dice.Length];

        InitializeColors(theme, result.State);
        InitializeScoringFlags(result.ScoringDice);
    }

    private void InitializeColors(Theme theme, RollState state)
    {
        _scoringColor = theme.ScoringDiceColor;
        _boringColor = theme.BoringDiceColor;
        _textColor = theme.GameMasterVoiceColor;
        _text = "";

        switch(state)
        {
            case RollState.Farkled:
                _text = "🍒🍋🍒 Farkle 🍒🍋🍒";
                _textColor = theme.FarkleColor;
                break;
            case RollState.Hot:
                _text = "🔥 Hot Dice 🔥";
                _textColor = theme.HotDiceColor;
                _scoringColor = theme.HotDiceColor;
                break;
            default:
                break;
        }
    }

    private void InitializeScoringFlags(DiceTray scoringDice)
    {
        var currentTray = scoringDice;

        for(int i = 0; i < _dice.Length; i++)
        {
            var currentDieValue = _dice[i].CurrentValue;
            if (currentTray.DieCount(currentDieValue) > 0)
            {
                currentTray -= new DiceTray(new DieValueCount(currentDieValue, 1));
                _scoringFlags[i] = true;
            }
        }
    }

    public string Build()
    {
        var sb = new StringBuilder();

        sb.AppendLine(BuildLine1());
        sb.AppendLine(BuildLine2());
        sb.Append(BuildLine3());
        sb.Append($"[{_textColor}]");
        sb.AppendLine(SPACER + _text);
        sb.Append("[/]");
        sb.AppendLine(BuildLine4());
        sb.Append(BuildLine5());

        return sb.ToString();
    }

    // "+-----+  +-----+  +-----+  +-----+  +-----+  +-----+"
    // "|     |  | o   |  | o   |  | o o |  | o o |  | o o |"
    // "|  o  |  |     |  |  o  |  |     |  |  o  |  | o o |"
    // "|     |  |   o |  |   o |  | o o |  | o o |  | o o |"
    // "+-----+  +-----+  +-----+  +-----+  +-----+  +-----+"

    private string BuildLine1()
    {
        var segments = new List<string>();

        for (var i = 0; i < _dice.Length; i++)
            segments.Add(GetDieColorTag(i) + TOP_LINE + CLOSE_TAG);

        return string.Join(SPACER, segments);
    }

    private string GetDieColorTag(int dieIndex)
    {
        var color = _scoringFlags[dieIndex] ? _scoringColor : _boringColor;
        return $"[{color}]";
    }

    private string BuildLine2()
    {
        var segments = new List<string>();

        for (var i = 0; i < _dice.Length; i++)
        {
            var die = _dice[i];
            var tag = GetDieColorTag(i);

            switch(die.CurrentValue)
            {
                case 1:
                    segments.Add(tag + NO_PIP + CLOSE_TAG);
                    break;
                case 2:
                case 3:
                    segments.Add(tag + LEFT_PIP + CLOSE_TAG);
                    break;
                default:
                    segments.Add(tag + DOUBLE_PIP + CLOSE_TAG);
                    break;
            }
        }

        return string.Join(SPACER, segments);
    }

    private string BuildLine3()
    {
        var segments = new List<string>();

        for (var i = 0; i < _dice.Length; i++)
        {
            var die = _dice[i];
            var tag = GetDieColorTag(i);

            switch(die.CurrentValue)
            {
                case 1:
                case 3:
                case 5:
                    segments.Add(tag + CENTER_PIP + CLOSE_TAG);
                    break;
                case 2:
                case 4:
                    segments.Add(tag + NO_PIP + CLOSE_TAG);
                    break;
                default:
                    segments.Add(tag + DOUBLE_PIP + CLOSE_TAG);
                    break;
            }
        }

        return string.Join(SPACER, segments);
    }

    private string BuildLine4()
    {
        var segments = new List<string>();

        for (var i = 0; i < _dice.Length; i++)
        {
            var die = _dice[i];
            var tag = GetDieColorTag(i);

            switch(die.CurrentValue)
            {
                case 1:
                    segments.Add(tag + NO_PIP + CLOSE_TAG);
                    break;
                case 2:
                case 3:
                    segments.Add(tag + RIGHT_PIP + CLOSE_TAG);
                    break;
                default:
                    segments.Add(tag + DOUBLE_PIP + CLOSE_TAG);
                    break;
            }
        }

        return string.Join(SPACER, segments);
    }

    private string BuildLine5()
    {
        var segments = new List<string>();

        for (var i = 0; i < _dice.Length; i++)
            segments.Add(GetDieColorTag(i) + BOTTOM_LINE + CLOSE_TAG);

        return string.Join(SPACER, segments);
    }
}

using Spectre.Console;
using Domain;

namespace Cli;

public class GameRunner
{
    private readonly Game _game;
    private readonly Theme _theme;
    private int _rollScore = 0;
    private Player _player;


    public GameRunner(Game game, Theme theme)
    {
        _game = game ?? throw new ArgumentNullException(nameof(game));
        _theme = theme ?? new Theme();
        _player = _game.CurrentPlayer;
    }

    public GameDisposition Run()
    {
        WriteBeginTurn(_game.CurrentPlayer);

        var disposition = ExecuteGameLoop();

        HandleGameOver();

        return disposition;
    }

    private GameDisposition ExecuteGameLoop()
    {
        char command;

        do
        {
            command = PromptForCommand();

            HandleCommand(command);

            _player = _game.CurrentPlayer;
        } while (command != 'q' && !_game.IsOver);

        return (command == 'q')
            ? GameDisposition.Quit
            : GameDisposition.Finished;
    }

    private void HandleCommand(char command)
    {
        switch (command)
        {
            case 'q':
                HandleQuit();
                break;
            case 'r':
                HandleRoll();
                break;
            case 'b':
                HandleBank();
                break;
            case 's':
                HandleScoreboard();
                break;
            case 'm':
                HandleMeldList();
                break;
            case 'h':
            case '?':
                HandleHelp();
                break;
            default:
                HandleUnrecognizedInput();
                break;
        }
    }

    private char PromptForCommand()
    {
        string prompt = "";

        if (_game.SuddenDeath)
        {
            var needed = _game.TopScore - _player.Score;
            prompt +=
                $"[{_theme.SuddenDeathColor}]You need [/]" +
                $"[{_theme.SuddenDeathScoreColor}]{needed:N0}[/]" +
                $"[{_theme.SuddenDeathColor}] points for first place.[/]\n";
        }

        prompt +=
            $"Points at Risk: [{_theme.TurnScoreColor}]{_game.TurnScore + _rollScore:N0}[/] " +
            $"[[[{_theme.CommandColor}]R[/]]]oll";

        if (_game.CanBank)
            prompt += $" or [[[{_theme.CommandColor}]B[/]]]ank?";

        return AnsiConsole.Ask<string>(prompt).ToLower().FirstOrDefault();
    }

    private void HandleUnrecognizedInput()
    {
        AnsiConsole.MarkupLine($"Huh? [{_theme.GameMasterVoiceColor}]Try Again.[/]");
    }

    private void HandleQuit()
    {
        AnsiConsole.MarkupLine($"[{_theme.GameMasterVoiceColor}]Bye![/]");
    }

    private void HandleBank()
    {
        if (!_game.CanBank)
        {
            HandleUnrecognizedInput();
            return;
        }

        _game.Bank();
        _rollScore = 0;

        WriteEndTurn(_player);

        if (_game.IsOver)
            return;
    
        if (_game.SuddenDeath)
            WriteSuddenDeath();

        WriteBeginTurn(_game.CurrentPlayer);
    }

    private void HandleRoll()
    {
        var result = _game.Roll();
        _rollScore = result.MaxScore;

        WriteDice(result);

        if (result.State == RollState.Farkled)
        {
            WriteEndTurn(_player);
            if (!_game.IsOver)
                WriteBeginTurn(_game.CurrentPlayer);
        }
    }

    private void HandleGameOver()
    {
        var rule = new Rule($"[{_theme.SeparatorValueColor}]GAME OVER[/]")
        {
            Style = Style.Parse(_theme.SeparatorColor),
            Alignment = Justify.Left
        }
        .Centered();

        AnsiConsole.Write(rule);

        HandleScoreboard();
    }

    private void HandleScoreboard()
    {
        var builder = new ScoreboardBuilder(_game.Players, _game.IsOver, _theme.SeparatorValueColor);

        Table table = builder.Build();

        AnsiConsole.Write(table);
    }

    private void HandleMeldList()
    {
        var builder = new RulesTableBuilder(_game.Rules, _theme.SeparatorValueColor);

        Table table = builder.Build();

        AnsiConsole.Write(table);
    }

    private void HandleHelp()
    {
        var table = new Table();
        table.Border(TableBorder.Rounded);
        table.HideHeaders();

        table.AddColumn("Command");
        table.AddColumn("Description");

        table.AddRow($"[{_theme.CommandColor}]H[/], [{_theme.CommandColor}]?[/]", $"Get Help. [{_theme.GameMasterVoiceColor}](you are here)[/]");
        table.AddRow($"[{_theme.CommandColor}]S[/]", "View the Scoreboard.");
        table.AddRow($"[{_theme.CommandColor}]M[/]", "List the scoring rules (Melds).");
        table.AddRow($"[{_theme.CommandColor}]Q[/]", "Quit the game.");
        table.AddRow($"[{_theme.CommandColor}]R[/]", "Roll the dice. If you score a 0 (farkle), you lose your at risk points and end your turn.");
        table.AddRow($"[{_theme.CommandColor}]B[/]", "Bank your points. Add your at risk points to your score and end your turn.");

        AnsiConsole.Write(table);
    }

    private bool _suddenDeathWritten;
    private void WriteSuddenDeath()
    {
        if (_suddenDeathWritten)
            return;

        WriteRule(_theme.SeparatorColor);

        var rule = new Rule("💀 SUDDEN DEATH 💀")
        {
            Style = Style.Parse($"{_theme.SuddenDeathColor}"),
            Alignment = Justify.Center
        };
        AnsiConsole.Write(rule);
        _suddenDeathWritten = true;
    }

    private static void WriteRule(string color)
    {
        var rule = new Rule()
        {
            Style = Style.Parse(color),
        };

        AnsiConsole.Write(rule);
    }

    private void WriteEndTurn(Player player)
    {
        var message = $"[{_theme.SeparatorLabelColor}] Current Score:[/] [{_theme.SeparatorValueColor}]{player.Score:N0}[/]\n";

        AnsiConsole.MarkupLine(message);
    }

    private void WriteBeginTurn(Player player)
    {
        var scoreText =
            $"[{_theme.SeparatorLabelColor}]Player:[/] " +
            $"[{_theme.SeparatorValueColor}]{player.Name}[/] " +
            $"[{_theme.SeparatorLabelColor}]Current Score:[/] " +
            $"[{_theme.SeparatorValueColor}]{player.Score:N0}[/]";

        var rule = new Rule(scoreText)
        {
            Style = Style.Parse(_theme.SeparatorColor),
            Alignment = Justify.Left
        };

        AnsiConsole.Write(rule);
    }

    private void WriteDice(RollResult result)
    {
        var diceString = new DiceArtStringBuilder(_theme, result).Build();

        AnsiConsole.MarkupLine(diceString);
    }
}
